# -*- coding: utf-8 -*-
"""
Created on Fri May  3 10:13:56 2024

@author: tobia
"""


import numpy as np
from test_few_spins_2D import MySystem
import time
import os
import winsound

mute_plotting = False
x_tot = []

save_dir = "C:/Users/Tobias/OneDrive/Desktop/ETH/MATL/polybox/CMSM_skyrmions/AR_1.77_N84"
t0 = time.time()

aspect_ratio = np.sqrt(3)
n=84
X = 3
K_mag = np.arange(0.0,3,0.25) 
sweep = np.arange(0.0,1.75,0.25)

num_sweep=len(sweep)
print('Doing', num_sweep,'sweeps over H')
a = 2
                      #lattice constant, arbitrairy length
N = n*np.ones(num_sweep)                           #number of spins
                  
J = np.ones(num_sweep)*1/2                              #magnitude, exchange interaction between nearest-neigh's       


dmi_mag = np.ones(num_sweep)                      #magnitude of DMI interaction, only give this, the direction is hardcoded in the Hamiltonian

H_mag =  0.71**2*dmi_mag**2/J*sweep                     # magnitudeof external field 
H_dir= np.array([0,0,1]) 

for xi in range(X-1,X):
    
    
 # will define direction of skyrmion, stabilizes skyrmion 
                                        # should be  [0,0,1] or [0,0,-1]
                                        
    K = np.ones(num_sweep)*(K_mag[xi])                           #magnitnude, (magneto-crystalline) anisotropy
    easy_ax = np.array([0,0,1])         #Easy axis (System minimizes energy by aligning spins to this axis)
    
    
    KJDsq = K*J/(dmi_mag**2)
    HJDsq = H_mag*J/(dmi_mag**2)
    print(HJDsq)
    print("KJ/D^2 %.2f" % KJDsq[0])
    print("HJ/D^2 %.2f" % HJDsq[0])
    folder_name = "vert_sweep_%.2f_2_in_FM" %KJDsq[xi]
    file_path= os.path.join(save_dir,folder_name)
    if not os.path.exists(file_path):
        os.makedirs(file_path)
    for ii in range(len(dmi_mag)):
        
        H_field = H_mag[ii]*H_dir
        #print("\n Running first configuration with N=",N[ii]," J=",J[ii]," K=", K[ii]," DMI_mag=", dmi_mag[ii]," H=", H_field, "\n")
        KJDsq =K[ii]*J[ii]/(dmi_mag[ii]**2)
        HJDsq = H_mag[ii]*J[ii]/(dmi_mag[ii]**2)
        print("KJ/D^2",KJDsq,"\n")
        print("HJ/D^2",HJDsq)
        I = MySystem(N[ii], a, J[ii], K[ii], dmi_mag[ii], H_field, easy_ax,aspect_ratio,file_path,mute_plotting)
        I.Setup_2D(set_type= "1_in_FM",r=0) #Setup 1 downspin in a chain of upspins
        I.matshow()
        I.minimize(tol = 1e-3)
        print("Progress: ",ii/num_sweep*100, "%")
        tit = f"N{N[ii]}_HJ-Dsq%.2f_KJ-Dsq%.2f" %(HJDsq,KJDsq)
        I.plot_config2D(I.spins,save = True,title = tit)
        I.savedata()
    
print(np.round(time.time()-t0)/60, "minutes used")
for  i in range(3):
    winsound.PlaySound("SystemExclamation", winsound.SND_ALIAS)
#%%
#I.plot_config3D(inst[-1].spins)

