# -*- coding: utf-8 -*-
"""
Created on Mon Apr 22 13:21:16 2024

@author: tobia
"""

import scipy.optimize as sop
import numpy as np
from test_few_spins_2D import MySystem
import time
import os

a = 2                               #lattice constant, arbitrairy length

n_0 = 3
n_max = 15
L = n_max - n_0 + 1                 # number of iterations
n = np.linspace(n_0,n_max,L)        # array of number of spins per side
#N = n**2                            #number of spins                   
J = np.ones(L)                               #magnitude, exchange interaction between nearest-neigh's
K = np.ones(L)*0                               #magnitnude, (magneto-crystalline) anisotropy
dmi_mag = np.ones(L)*2                         #magnitude of DMI interaction, only give this, the direction is hardcoded in the Hamiltonian

H_mag = np.ones(L)*2                        # magnitudeof external field 
H_dir = np.array([0,0,-1])  # will define direction of skyrmion, stabilizes skyrmion 
                                    # should be [0,0,1] or [0,0,-1]
easy_ax = np.array([0,0,1])         #Easy axis (System minimizes energy by aligning spins to this axis)

t0 = time.time()

save_dir = "/Users/laurawindlin/Library/CloudStorage/OneDrive-ETHZurich/1 - ETH/3 - MSc/3 - Computational Modeling/skyrmions"

inst = []

KJDsq = K*J/(dmi_mag**2)
HJDsq = H_mag*J/(dmi_mag**2)
#print(HJDsq)
#print("KJ/D^2 %.2f" % KJDsq[0])
#print("HJ/D^2 %.2f" % HJDsq[0])
folder_name = "size_sweep_oneflippedspin"
file_path= os.path.join(save_dir,folder_name)
if not os.path.exists(file_path):
    os.makedirs(file_path)

for ii in range(len(dmi_mag)):
    H_field = H_mag[ii]*H_dir
    print("size \n", n[ii])
    N = n[ii]**2
    I = MySystem(N, a, J[ii], K[ii], dmi_mag[ii], H_field, easy_ax, file_path)
    I.Setup_2D(set_type="1_in_FM")
    dt, ret = I.minimize(tol= 1e-3)
    print("Progress: ", ii/L*100,"%")
    tit = f"{n[ii]}^2 spins"
    I.plot_config2D(I.spins, save=True, title = tit)
    I.savedata()
    #print("E initial config\n" ,I.Hamiltonian_pbc(x0))
    print("E final config\n" ,ret.fun)
    E_final_norm = ret.fun/N
    print("E final normalised\n", E_final_norm)
    inst = np.append(inst,I)

print(np.round(time.time()-t0)/60, "minutes used")

#x0 = I.s0       #initial config of spins, defined by Setup_1D
#x0[2] +=0.001   #would be trapped in local minima
#print("Cartesian coordinates of the initial config: \n")
#print("everything has started lets gooo")
#print(np.round(I.sph_to_car2D(x0)),"\n")

#I.minimize(tol = 1e-3)


#I.savedata()

#ret = sop.minimize(I.Hamiltonian_pbc, x0, method='BFGS', tol = 1e-3) #method='CG' uses the steepest gradient method for minimisation


#I.plot_config2D(ret.x)
#tit = f"N{N[ii]}_HJ-Dsq%.2f_KJ-Dsq%.2f" %(HJDsq,KJDsq)
#I.plot_config2D(ret.x,save = True,title = tit)
#I.plot_config3D(ret.x)

    

"""   
#print("E(initial config)\n" ,I.Hamiltonian_pbc(I.spins))
#I.plot_config(I.spins) #If there was no change between initial and current positions -> plot only initial configuration


#dr = np.reshape(dr,(self.N,self.N,3))
#self.force=np.sum( np.multiply(pref[:,:,None],dr),axis=0)

I2 = MySystem(N, a, J, K, dmi_mag, H_field, easy_ax)
I2.Setup_2D(set_type= "FM") #Setup all spins up



x0 = I.s0       #initial config of spins, defined by Setup_1D
x0[1] -=0.02   #would be trapped in local minima
print("Cartesian coordinates of the initial config: \n")
print(np.round(I.sph_to_car2D(x0)),"\n")
print("E(initial config)\n" ,I.Hamiltonian_pbc(x0))
print("E(FM)\n" ,I2.Hamiltonian_pbc(I2.s0))
ret = sop.minimize(I.Hamiltonian_pbc, x0, tol = 1e-3)
#print(ret)

#I.plot_config(ret.x)   

''' 
a = 2
N = 5
J = 0.5
K = 0.1
dmi_mag = 0
dmi = dmi_mag*np.array([0,1,0])
easy_ax = np.array([1,0,0]) #test different easy axis!


I = MySystem(N, a, J, K, dmi, easy_ax)
I.Setup_2D(set_type= "FM")

#Here I do not minimize (find relaxed config), since FM should already be the relaxed config (try minimize this config to check?)

####
print("E(initial config)\n" ,I.Hamiltonian_pbc(I.spins))
FM = I.spins

#I.plot_config(I.spins) #If there was no change between initial and current positions -> plot only initial configuration
'''

"""

                    
        
        