# -*- coding: utf-8 -*-
"""
Created on Fri May 17 09:05:22 2024

@author: droux
"""

import scipy.optimize as sop
import numpy as np
from test_few_spins_2D import MySystem
import time
import os

mute_plotting = False
x_tot = []
save_dir = "C:/Users/droux/OneDrive - ETH Zurich/Dokumente/ETH/MS_FS_2/Computational_multiscale_modelling"
t0 = time.time()

aspect_ratio = np.sqrt(3)
N=(60,84,112)       #10:6,12:7,14:8,15:9   SYSTEMSIZE, CHANGING  <<----
#N=[60,84,112,135]
KJDsq = -1.125    #  CHANGE HERE <<-----
HJDsq = 2.125      #HJ/D^2: for loop will choose this value and iterate +/- 0.25  <<---- CHANGE HERE

#
a = 2                           #lattice constant, arbitrairy length
J = 0.5                         #magnitude, exchange interaction between nearest-neigh's       
dmi_mag = 1                     #magnitude of DMI interaction, only give this, the direction is hardcoded in the Hamiltonian
H_dir= np.array([0,0,1])
H_mag = (HJDsq*(dmi_mag**2)/J)   
K = KJDsq*(dmi_mag**2)/J
easy_ax = np.array([0,0,1])    

folder_name = "boundarycheck,_%.2f_2_in_FM" %KJDsq
file_path= os.path.join(save_dir,folder_name)
if not os.path.exists(file_path):
    os.makedirs(file_path)
    
for ii in range(1):         #range from given H, go for (2)-> 0, +0.25 or -1,0,+1 (-1,1) 
    H_magi = H_mag + ii*0.25 
    HJDsq = H_magi*J/(dmi_mag**2)
    H_field = H_magi*H_dir
    
    for sizei in range (len(N)):        #goes over different system sizes given above, with N                                
        print(HJDsq)
        print("KJ/D^2 %.2f" % KJDsq)
        print("HJ/D^2 %.2f" % HJDsq)
        print("size n=", N[sizei])
        I = MySystem(N[sizei], a, J, K, dmi_mag, H_field, easy_ax,aspect_ratio,file_path,mute_plotting)
        I.Setup_2D(set_type= "1_in_FM",r=0) #Setup 1 downspin in a chain of upspins
        #I.matshow()
        I.minimize(tol = 1e-3)
        #print("Progress: ",ii/num_sweep*100, "%")
        tit = f"_HJ-Dsq%.2f_KJ-Dsq%.2f_N %.2f" %(HJDsq,KJDsq,N[sizei])
        I.plot_config2D(I.spins,save = True,title = tit)
        I.savedata()
        
print(np.round(time.time()-t0)/60, "minutes used")