# -*- coding: utf-8 -*-
"""
Created on Thu May  9 17:26:58 2024

@author: tobia
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt 

import matplotlib.patches as mpatches
# Given CSV file path
#csv_file_path = "C:/Users/Tobias/OneDrive/Desktop/ETH/MATL/polybox/CMSM_skyrmions/9x9/1_in_FM.txt"
csv_file_path = "C:/Users/tobia/OneDrive/Desktop/Downloads/-1,1_PD.txt"
save_path = "C:/Users/tobia/OneDrive/Desktop/Downloads"
#save_path = "C:/Users/Tobias/OneDrive/Desktop/ETH/MATL/polybox/CMSM_skyrmions/9x9"
data = pd.read_table(csv_file_path)
PD = data.iloc[:,1::]
PD = PD.fillna(0)
x = np.flip(data.columns.values[1::])
y = data.iloc[:,0]

def highlight_cell(x,y,dx,dy, ax=None, **kwargs):
    rect = plt.Rectangle((x-.5, y-.5), dx,dy, fill=False, **kwargs)
    ax = ax or plt.gca()
    ax.add_patch(rect)
    return rect

fig = plt.figure(dpi = 400)
ax = fig.add_subplot(111)
im = ax.matshow(np.flip(PD.values,axis=1))

values = np.unique(PD.values.ravel())
# get the colors of the values, according to the 
# colormap used by imshow
colors = [ im.cmap(im.norm(value)) for value in values]
# create a patch (proxy artist) for every color 
Label = ['FM','SK','SP']
patches = [ mpatches.Patch(color=colors[i], label=Label[i].format(l=values[i]) ) for i in range(len(values)) ]
# put those patched as legend-handles into the legend
plt.legend(handles=patches, bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0. )
highlight_cell(0,2,6,7, color="red", linewidth=2)
ax.set_aspect(0.6)
xaxis = np.arange(len(x))
yaxis = np.arange(len(y))
ax.tick_params(axis = 'x', rotation = 40)
ax.set_xticks(xaxis)
ax.set_yticks(yaxis)
ax.set_xticklabels(x)
ax.set_yticklabels(y)
plt.title(r'$9\times9$ Systemsize, Bloch-type $SO(2)?$')
plt.ylabel(r'$\frac{HJ}{D^2}$',fontsize = 14)
plt.xlabel(r'$\frac{KJ}{D^2}$',fontsize = 14)
plt.tight_layout(w_pad=-0.1,h_pad=0.2)

plt.savefig(save_path+"/1_in_FM.png", dpi = 400)
plt.show()