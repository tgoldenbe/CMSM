# -*- coding: utf-8 -*-
"""
Created on Wed Jun 19 20:16:18 2024

@author: tobia
"""


import scipy.optimize as sop
import numpy as np
from test_few_spins_2D import MySystem
import os

mute_plotting = False   #if true, will not ouput plots during run, just save time
save_dir = "Define folder here with / instead of \"
 
a = 2                               #lattice constant, arbitrairy length
N = 84                             #number of spins                   
J = 0.5                               #magnitude, exchange interaction between nearest-neigh's
K = -1.5                            #magnitnude, (magneto-crystalline) anisotropy
dmi_mag = 1                        #magnitude of DMI interaction, only give this, the direction is hardcoded in the Hamiltonian
aspect_ratio = np.sqrt(3)
easy_ax = np.array([0,0,1])         #Easy axis (System minimizes energy by aligning spins to this axis)
H_mag =  1.5                        # magnitudeof external field 
H_dir = np.array([0,0,1])  # will define direction of skyrmion, stabilizes skyrmion 
H_field = H_mag*H_dir


KJDsq = K*J/(dmi_mag**2)
HJDsq = H_mag*J/(dmi_mag**2)
print("HJ/Dsq",HJDsq)
print("KJ/Dsq",KJDsq)
folder_name = "vert_sweep_%.2f_2_in_FM" %KJDsq
file_path= os.path.join(save_dir,folder_name)
if not os.path.exists(file_path):
    os.makedirs(file_path)
                                    # should be [0,0,1] or [0,0,-1]

I = MySystem(N, a, J, K, dmi_mag, H_field, easy_ax,aspect_ratio,file_path,mute_plotting)
I.Setup_2D(set_type= "1_in_FM",r=0) #Setup downspins in a sea of upspins (FM), favours SKx generation
I.minimize(tol = 1e-3)

tit = f"N{N}_HJ-Dsq%.2f_KJ-Dsq%.2f" %(HJDsq,KJDsq)
I.plot_config2D(I.spins,save = True,title = tit)
I.savedata()

