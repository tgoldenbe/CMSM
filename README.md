# CMSM



## Getting started

1.	Download git bash for your operating system. https://git-scm.com/downloads
2.	Navigate to directory (folder) where to download (copy) the project files to. Right click and open “git bash”. On mac open a terminal in the folder where to download git files.
3.	Connect to the project via https: ```git clone https://gitlab.ethz.ch/tgoldenbe/CMSM.git```
4.	Change directory to downloaded directory (cd CMSM). This is where you modify/upload etc your local files...


## Change to a specific branch:

1.	Usually people pushed changed files. First collect the newest versions of all branches, before switching to them using: 
```git pull origin```
2.	Switch branch with: ```git checkout branch``` (e.g. git checkout Tobias or git checkout main)

All local files should change accordingly. If everything works well, changes should immediately be updated in your IDE (e.g. jupyter, spyder etc.) without opening the file again.


## Upload changes to local files.

1.	Add all local files to a new version which one is about to upload: 
```git add .```
The dot indicates all files, could also specify which files
2.	Create a commit (new version) with a description: 
```git commit –m “type message here (mit Gänsefüsschen)”```
3.	Finish uploading the files and commit to gitlab: 
```git push origin branch``` (e.g. git push origin main or git push origin Tobias)
4.	Usually you are prompted to authenticate: Use your ethz ID (ID@student.ethz.ch) and the token to the project above (check whatsapp)

Check online, all the files as they are right now on your local machine, should be the same online on selected branch and same commit.


Here follow generic suggestions from GitLab:
## Add your files


- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:


## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.ethz.ch/tgoldenbe/CMSM/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***


>>>>>>> main
